FROM maxmindinc/geoipupdate

ENV CORE_PACKAGES="openssl ca-certificates bash curl git python3 jq bc" \
    GCLOUD_ARCHIVE=google-cloud-sdk-459.0.0-linux-x86_64.tar.gz
ENV PATH /google-cloud-sdk/bin:$PATH
WORKDIR /
RUN apk --no-cache add $CORE_PACKAGES && \
    curl -O https://storage.googleapis.com/cloud-sdk-release/$GCLOUD_ARCHIVE && \
    tar -xf $GCLOUD_ARCHIVE && \
    rm $GCLOUD_ARCHIVE
RUN gcloud components install kubectl gke-gcloud-auth-plugin
RUN wget -O -  https://get.acme.sh | sh
COPY ./git-repo-watcher* /usr/local/bin/
