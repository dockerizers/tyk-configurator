# Tyk Configurator

Docker image with packages to set up Geoipdb and Acme SSL. Also contains google-cloud-sdk and a git-repo-watcher to track updates to a git repository.

## Packages
1. [geoipupdate](http://dev.maxmind.com/geoip/geoipupdate/) - download and update maxmind GeoIP databases.
   Command available in `$PATH` as `geoipupdate`

   Sample usage;
   ```bash
   geoipupdate -f [config file] -d [/path/to/databases]
   ```
2. [acme.sh](https://github.com/acmesh-official/acme.sh) - issue and renew Let's Encrypt SSL certificates.
   Script installed to `/root/.acme.sh/acme.sh`

   Sample usage;
   ```bash
   /root/.acme.sh/acme.sh --issue --dns dns_gcloud \
      -d example.com \
      -d www.example.com
   ```
3. [git-repo-watcher](https://github.com/kolbasa/git-repo-watcher) - watch for changes to a git repo and perform actions based on those changes.
   See `git-repo-watcher-hooks` for example change events.

   Sample usage;
   ```bash
   git-repo-watcher -d "/path/to/your/repository" -h "/path/to/your/hooks-file"
   ```
4. [google cloud sdk](https://cloud.google.com/sdk/install) - with `gcloud`,`gsutil`,`bq` and `kubectl` commands.
   Available in `$PATH`

## Compose example:

``` yaml
version: "3.7"
services:
    tyk-config:
      build: .
      image: registry.gitlab.com/dockerizers/tyk-configurator:v0.0.1
      container_name: tyk-config
      # override default entrypoint. Default downloads geoip databases to /geodata
      # entrypoint: ["bash", "-c", "/scripts/entrypoint.sh"]
      command: ["/etc/conf/GeoIP.conf"] # config file for default entrypoint
      environment:
      volumes:
        - ./GeoIP.conf:/etc/conf/GeoIP.conf
        - ./geoipdb:/geodata
        # - ./entrypoint.sh:/scripts/entrypoint.sh
        # - ./hooks:/scripts/hooks
        # - ./repo:/repo
      restart: always
```
